angular.module('postsApp')
    .directive('mainContent', function() {
        return {
            restrict: 'E',
            templateUrl: 'app/templates/mainContent.html',
            scope: {},
            controller: 'mainContentCtrl'
        }
    });