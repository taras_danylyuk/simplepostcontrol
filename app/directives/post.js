angular.module('postsApp')
    .directive('post', function() {
        return {
            restrict: 'E',
            templateUrl: 'app/templates/post.html',
            scope: false,
            controller: 'postCtrl'
        }
    });