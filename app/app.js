angular.module('postsApp', ['ngCookies'])
    .run(['$http', '$cookies', function($http, $cookies) {

        /*Get access token for next requests*/
        this.getAccessToken = function() {
            $http.get('https://graph.facebook.com/oauth/access_token?client_id=263352124024833&client_secret=82e4be67a90a191a04e6a342fee5c803&grant_type=client_credentials').then(function (response) {
                $cookies.put('accessToken', response.data);
            }, function(error) {
                console.error('Can`t get access token', error);
            });
        };
        this.getAccessToken();
    }]);