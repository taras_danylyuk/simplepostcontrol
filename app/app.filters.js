angular.module('postsApp')
    .filter('search', function() {
        /*Search in author of post and message*/
        return function(input, searchValue) {
            /*Search only after three entered characters*/
            if(!searchValue || searchValue.length < 3) return input;

            var output = [];
            if(input) {
                input.forEach(function(element) {
                    var nameLower = element.from.name.toLowerCase();
                    var messageLower = element.message ? element.message.toLowerCase() : '';

                    if( nameLower.indexOf(searchValue) != -1 || messageLower.indexOf(searchValue) != -1) {
                        output.push(element);
                    }
                });
                return output;
            }
        }
    });