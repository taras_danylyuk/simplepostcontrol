angular.module('postsApp')
    .controller('mainContentCtrl', ['facebookService', '$scope', function (facebookService, $scope) {
        $scope.orderField = '-created_date';
        $scope.limitPosts = 25;
        $scope.posts = [];

        $scope.showPosts = function(from) {
            var pageId;
            if(from == 'if') {
                pageId = '156514317803943';
            }
            else if(from == 'google') {
                pageId = '104958162837'
            }

            facebookService.getAllUserPosts(pageId, function (response) {
                $scope.posts = response.data;
            });
        };
        $scope.showPosts('if');

        $scope.showMore = function() {
            if($scope.limitPosts < 100) $scope.limitPosts += 25;
        }
    }]);