angular.module('postsApp')
    .service('facebookService', ['$http', '$cookies', function($http, $cookies) {

        this.getAllUserPosts = function(id, callback) {
            $http.get('https://graph.facebook.com/v2.6/' + id + '/feed?fields=link,place,message, created_time, from&limit=100&' + $cookies.get('accessToken')).then(function(response) {
                callback(response.data);
            }, function(error) {});
        }

    }]);